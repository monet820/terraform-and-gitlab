terraform {
  backend "gcs" {
    bucket = "terraform-gcp-256910-tfstate"
    credentials = "~/terraform-and-gitlab/credentials/service_account.json"
  }
}