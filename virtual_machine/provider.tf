provider "google" {
    project = "terraform-gcp-256910"
    region = "europe-west-2-a"
    zone        = "europe-west4-a"
    credentials = file("~/terraform-and-gitlab/credentials/service_account.json")
}
