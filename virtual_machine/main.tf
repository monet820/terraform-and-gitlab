# Resource
resource "google_compute_instance" "default" {
count = length(var.name_count)

name         = "list-${count.index+1}"
machine_type = var.machine_type["standard-1"]

# Descriptive information about "default"
description = "This is our Virtual Machine"

# Firewall tags
tags = ["allow-http", "allow-https"]

# What to do with the VM on boot
boot_disk {
    initialize_params{
    image = var.image
    # size in gigabites of harddrive
    size = "10"
    }
}

labels = {
    name         = "list-${count.index+1}"
    machine_type = var.machine_type["standard-1"]
}

network_interface {
    network = "default"

    access_config {
      // Include this section to give the VM an external ip address
    }
}

metadata = {
    # enable-oslogin = "TRUE"
    ssh-keys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
    size = "10"
    foo = "bar"
}

# startup script
metadata_startup_script = "../scripts.sh"
}

resource "google_compute_disk" "default" {
name = "test-desk"
type = "pd-ssd"
size = "10"
# zone = ""
}

resource "google_compute_attached_disk" "default" {
    disk = google_compute_disk.default.self_link
    instance = google_compute_instance.default[0].self_link
    }