
#Variables used for files sharing same parent folder.variable

variable "image" {default = "ubuntu-os-cloud/ubuntu-1604-lts"}
variable "machine_count" {default = "1"}
variable "gce_ssh_pub_key_file" {default = "~/.ssh/id_rsa.pub"}
variable "gce_ssh_user" {default = "henrirbj"}

variable "name_count" {default = ["server1","server2","server3"] }

variable "machine_type" {
    type = map
    default = {
        standard-1 = "n1-standard-1"
        prod = "f1-micro"
    }
}
